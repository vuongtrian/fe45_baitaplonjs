document
  .getElementById("btnThemDienThoai")
  .addEventListener("click", openThemDienThoai);

function openThemDienThoai() {
  document.getElementsByClassName("modal-footer")[0].innerHTML = `
    <button class="btn btn-success" data-dismiss="modal" onclick ="xuLyThemDienThoai()">Thêm</button>
  `;
}

// Chức năng "Thêm điện thoại" :
function xuLyThemDienThoai() {
  const name = document.getElementById("nameDT").value;
  const image = document.getElementById("imageDT").value;
  const description = document.getElementById("descriptionDT").value;
  const price = document.getElementById("priceDT").value;
  const inventory = document.getElementById("inventoryDT").value;
  const rating = document.getElementById("ratingDT").value;
  const type = document.getElementById("hangDienThoai").value;

  const dienThoai = new DienThoai(
    name,
    image,
    description,
    price,
    inventory,
    rating,
    type
  );

  themDienThoai(dienThoai).then(function (result1) {
    fetchDienThoai();
  });
}

var danhSachDienThoai = [];

// function xuLyLayDanhSachDienThoai() {
//   layDanhSachDienThoai().then(function (result) {
//     dtList = result.data;
//     console.log(result);
//   });
// }

// Lấy dữ liệu về
const fetchDienThoai = () => {
  axios({
    url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products",
    method: "GET",
  })
    .then(function (res) {
      // console.log(res);
      danhSachDienThoai = res.data;
      // renderHTML();
      findProduct();
      //   questionList = res.data;
      // lấy dữ liệu từ backend lên nó sẽ mất phương thức, nên phải map
      //lấy dữ liệu thành công xong rồi chạy thằng Render
    })
    .catch(function (erorr) {
      console.log(erorr);
    });
};

//Render giao diện ra html:
function renderHTML(arr) {
  arr = arr || danhSachDienThoai;
  var htmlContent = "";
  for (var i = 0; i < arr.length; i++) {
    var dienThoai = arr[i];
    htmlContent += `
<div class="col-4">
    <div class="card p-4 m-2">
            <img class="pb-3" style="height:280px;"
            src="${dienThoai.image}"
            class="w-100" alt="">
            <p class="tenDT">
            Tên điện thoại: ${dienThoai.name}
            </p>
            <p>
            Mã sản phẩm: ${dienThoai.id}
            </p>
            <p>
            Mô tả về điện thoại: ${dienThoai.description}
            </p>
            <p class="giaDT">
            Giá: ${dienThoai.price}
            </p>
            <p>
            Số Lượng: ${dienThoai.inventory}
            </p>
            <p>
            Đánh giá: ${dienThoai.rating}
            </p>
            <p>
            Hãng điện thoại: ${dienThoai.type}
            </p>
            <div class="row">
            <div class="col-8">
            <button class="btn btn-success btnAddToCart" onclick = "addProductToCart(${dienThoai.id})">Thêm vào giỏ hàng</button>
            </div>  
            <div class="col-2">
                <button class="btn btn-info rounded-circle" 
                style="cursor: pointer" data-toggle="modal"
                data-target="#myModal" data-id="${dienThoai.id}" ><i class="fa fa-pencil"></i></button>
            </div>
            <div class="col-2">
                <button class="btn btn-danger rounded-circle" 
                style="cursor: pointer" onclick ="xuLyXoaDienThoai(${dienThoai.id})"><i class="fa fa-trash"></i></button>
            </div>
        </div>
    </div>
</div>
      `;
  }
  document.getElementById("hienThiDS").innerHTML = htmlContent;
}

// Chức năng "Xóa điện thoại" :
function xuLyXoaDienThoai(id) {
  xoaDienThoai(id).then(function () {
    fetchDienThoai();
  });
}

// Chức năng Sửa - Cập Nhật
document.getElementById("hienThiDS").addEventListener("click", handleClickEdit);

function handleClickEdit(event) {
  // console.log("Edit");
  // console.log(event.target);
  const selected = event.target;
  const id = selected.getAttribute("data-id");
  console.log(id);
  if (id) {
    document.getElementsByClassName("modal-footer")[0].innerHTML = `
    <button class="btn btn-success" onclick ="xuLySuaDienThoai(${id})" data-dismiss="modal">Sửa</button>
  `;
    //data-dismiss="modal" : nhấn "Sửa" nó tự đóng cái modal

    layThongTinDienThoai(id).then(function (result) {
      const dienThoai = result.data;

      document.getElementById("nameDT").value = dienThoai.name;
      document.getElementById("imageDT").value = dienThoai.image;
      document.getElementById("descriptionDT").value = dienThoai.description;
      document.getElementById("priceDT").value = dienThoai.price;
      document.getElementById("inventoryDT").value = dienThoai.inventory;
      document.getElementById("ratingDT").value = dienThoai.rating;
      document.getElementById("hangDienThoai").value = dienThoai.type;
    });
  }
}

function xuLySuaDienThoai(id) {
  const name = document.getElementById("nameDT").value;
  const image = document.getElementById("imageDT").value;
  const description = document.getElementById("descriptionDT").value;
  const price = document.getElementById("priceDT").value;
  const inventory = document.getElementById("inventoryDT").value;
  const rating = document.getElementById("ratingDT").value;
  const type = document.getElementById("hangDienThoai").value;

  const dienThoai = new DienThoai(
    name,
    image,
    description,
    price,
    inventory,
    rating,
    type
  );

  capNhatDienThoai(id, dienThoai).then(function () {
    //Sau khi cập nhật thành công, gọi lại hàm xuLyLayDanhSachNguoiDung
    fetchDienThoai();
  });
}

//Chức năng tìm sản phẩm
const findProduct = function () {
  const foundedProduct = [];
  const keyword = document
    .getElementById("txtSearch")
    .value.trim()
    .toLowerCase();

  for (var i = 0; i < danhSachDienThoai.length; i++) {
    const currentProduct = danhSachDienThoai[i];

    var productName = currentProduct.name;
    productName = productName.toLowerCase();

    if (productName === keyword) {
      foundedProduct.push(currentProduct);
      break;
    }

    if (productName.indexOf(keyword) !== -1) {
      foundedProduct.push(currentProduct);
    }
  }
  renderHTML(foundedProduct);
  // document.getElementById("a-z").addEventListener("click", sortAZ(foundedProduct));
  // document.getElementById("z-a").addEventListener("click", sortZA(foundedProduct));
};

//Chức năng sắp xếp theo tên điện thoại
const sortAZ = function (danhSachDienThoai) {
  for (var i = 0; i < danhSachDienThoai.length; i++) {
    for (var j = 0; j < danhSachDienThoai.length; j++) {
      if (danhSachDienThoai[j].name > danhSachDienThoai[j + 1].name) {
        var temp = danhSachDienThoai[j];
        danhSachDienThoai[j] = danhSachDienThoai[j + 1];
        danhSachDienThoai[j + 1] = temp;
      }
    }
  }
};
// document.getElementById('searchingBar_sortProductName').addEventListener('change',function(event){console.log(event.target.value) });

const sort = function (tang) {
  console.log(tang);
};

// document.getElementById("a-z").addEventListener("click", sortProductString());

const sortZA = function (danhSachDienThoai) {
  for (var i = 0; i < danhSachDienThoai.length; i++) {
    for (var j = 0; j < danhSachDienThoai.length; j++) {
      if (danhSachDienThoai[j] < danhSachDienThoai[j + 1]) {
        var temp = danhSachDienThoai[j];
        danhSachDienThoai[j] = danhSachDienThoai[j + 1];
        danhSachDienThoai[j + 1] = temp;
      }
    }
  }
};

// document.getElementById("a-z").addEventListener("click", sortAZ(danhSachDienThoai));
// document.getElementById("z-a").addEventListener("click", sortZA(danhSachDienThoai));
// renderHTML(danhSachDienThoai);

//Lọc các sản phẩm của Iphone hoặc Samsung

const filterIphone = function () {
  const foundedIphone = [];
  for (var i = 0; i < danhSachDienThoai.length; i++) {
    var currentIphone = danhSachDienThoai[i];
    if (currentIphone.type === "Iphone" || currentIphone.type === "iphone") {
      foundedIphone.push(currentIphone);
    }
  }
  renderHTML(foundedIphone);
};

const filterSamsung = function () {
  const foundedSamsung = [];
  for (var i = 0; i < danhSachDienThoai.length; i++) {
    var currentSamsung = danhSachDienThoai[i];
    if (currentSamsung.type === "Samsung") {
      foundedSamsung.push(currentSamsung);
    }
  }
  renderHTML(foundedSamsung);
};

var cartList = [];
//Chức năng: Thêm sản phẩm vào giỏ hàng:

const addProductToCart = function (productID) {
  // var productID = danhSachDienThoai[i];
  // console.log(productID);
  // console.log(danhSachDienThoai);
  var id = parseFloat(productID);
  //parese 2 cái id về số: rồi đem so sánh
  for (var i = 0; i < danhSachDienThoai.length; i++) {
    // console.log(danhSachDienThoai[i].id);
    if (parseFloat(danhSachDienThoai[i].id) === id) {
      cartList.push(danhSachDienThoai[i]);
    }
  }
  console.log(cartList);
  // console.log(cartList);
  renderCart();
  saveData();
};

//Chức năng: render giỏ hàng:
function renderCart() {
  var htmlContentCart = "";
  for (var i = 0; i < cartList.length; i++) {
    var cartProduct = cartList[i];
    htmlContentCart += `
  <tr class="tableCart">
  <td><img style="width:120px; height:120px;" 
          src="${cartProduct.image}" /></td>
  <td style="font-size:25px;">${cartProduct.name}</td>
  <td>${cartProduct.price}</td>
  <td>
      1
      <div class="btn-group">
          <button class="btn btn-info border-right">-</button>
          <button class="btn btn-info border-left">+</button>
      </div>
  </td>
  <td>${cartProduct.price}</td>
  <td>
  <button class="btn btn-danger rounded-circle" 
  style="cursor: pointer" onclick ="removeProductInCart(${cartProduct.id})"><i class="fa fa-trash"></i></button>
  </td>
</tr>
  `;
  }
  document.getElementById("hienthiCart").innerHTML = htmlContentCart;
}

const findById = function (id) {
  for (var i = 0; i < cartList.length; i++) {
    if (cartList[i].id === id) {
      return i;
    }
  }
  return -1;
};

// const removeProductInCart = function (idRemove) {
//   var id = parseFloat(idRemove);
//   for (var i = 0; i < cartList.length; i++) {
//     if (parseFloat(cartList[i].id) === id) {
//       cartList.splice(id, 1);
//     }
//   }
//   // console.log(cartList);
//   console.log(cartList);
//   renderCart();
//   saveData();
// };

//Chức năng xóa Product giỏ hàng:
const removeProductInCart = function () {
  const index = findById();
  if (index !== -1) {
    cartList.splice(index, 1);
    renderCart();
    saveData();
  }
};

// -------- Chức năng lưu giỏ hàng vào local storage ------
//function 1: Lưu dữ liệu vào localstorage
const saveData = function () {
  localStorage.setItem("DScartList", JSON.stringify(cartList));
};
// JSON.stringify dùng để chuyển mảng thành 1 cái chuỗi
//function 2: Lấy dữ liệu từ localStorage
const getData = function () {
  const cartListJason = localStorage.getItem("DScartList");
  //Kiểm tra tồn tại dữ liệu
  if (!cartListJason) return;
  // if(cartListJason === null)

  //map từ mảng cũ [n1,n2.n3] --> [new productCart(n1), new productCart(n2), new productCart(n3)]
  const cartListFormLocal = JSON.parse(cartListJason);

  for (var i = 0; i < cartListFormLocal.length; i++) {
    //n1
    const currenCart = cartListFormLocal[i];
    // ==> new Cart(n1): giống hoàn toàn n1, chỉ khác là có phương thức, còn n1 thì k có
    const cartProduct = new DienThoai(
      currenCart.name,
      currenCart.image,
      currenCart.description,
      currenCart.price,
      currenCart.inventory,
      currenCart.rating,
      currenCart.type
    );
    //add vào giỏ hàng lai
    cartList.push(cartProduct);
  }

  // emplList = JSON.parse(cartListJason);
  // JSON.parse chuyển từ chuyển sang lại mảng
  //Tạo giao diện giỏ hàng
  renderCart();
};

// function tính Tổng Tiền đt:
const calcSum = function () {
  const sumMoney = 0;
  for (var i = 0; i < cartList.length; i++) {
    return (sumMoney += parseFloat(cartList[i].price));
    console.log(sumMoney);
  }
};

//Chức năng thanh toán clear Giỏ Hàng:
const clearCart = function () {
  cartList = [];
  alert("Bạn đã thanh toán thành công.");
  renderCart();
};
document.getElementById("btnThanhToan").addEventListener("click", clearCart);

fetchDienThoai();

getData();
