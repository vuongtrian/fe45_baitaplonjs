function layDanhSachDienThoai() {
  return axiosClient({
    method: "GET",
    url: "products",
  });
}

function themDienThoai(dienThoai) {
  return axiosClient({
    method: "POST",
    url: "products",
    data: dienThoai,
  });
}

function xoaDienThoai(id) {
  return axiosClient({
    method: "DELETE",
    url: `products/${id}`,
  });
}

function layThongTinDienThoai(id) {
  return axiosClient({
    method: "GET",
    url: `products/${id}`,
  });
}

function capNhatDienThoai(id, dienThoai) {
  return axiosClient({
    method: "PUT",
    url: `products/${id}`,
    data: dienThoai,
  });
}